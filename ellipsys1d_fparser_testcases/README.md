# Test case description

These test cases only tests the fparser model, no equations are solved.
All test cases consists of a line grid.

## Grid

The grid consists of 192 cells and the domain is 6 km tall. 

## Case 1: fparser/minmax

A linear profile is set for u, v, vis and den-den_0 that is bounded by a maximum
and a minimum value, both for the initial field conditions and inlet conditions.

## Case 2: fparser/sum

This test case is the same as Case 1, where a linear profile is added to the,
original values  for u, v, vis and den-den_0. This means that the inlet conditions
are increased every time step.

## Case 3: fparser/file

This test case is the same as Case 1, where a linear profile is set through a file 
for original values  for u, v.

## Case 4: fparser/functions

This test case test all possible function that can be used.
