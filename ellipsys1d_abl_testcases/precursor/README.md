# Precursor cases

These cases do *not* have analytical solutions and numerical simulations are therefore necessary.

## neutral_nocorirot

This test case calculates a neutral ABL including a prescribed
pressure gradient in form of the geostrophic wind $`G`$, and a
turbulent length scale limiter to limit the boundary layer height.
Although the solution is steady state, the solver needs to be run
in transient mode in order to converge, because the equations are
very stiff.

## neutral

This test case is the same as the neutral ASL case (from analytic_profiles), but including Coriolis forces.

## neutral_var_lmax

This test case is the same as the case above, but where $`\ell_{max}`$ follows a
periodic function with a 1 day period.

## unstable

This test case is the same as neutral test case above, but where unstable conditions are
set at the surface using a Buoyancy source term defined by MOST.

## gabls2

This test case is based on the work of Svensson et al.:
DOI 10.1007/s10546-011-9611-7
A variable wall temperature is set that reflects a diurnal cycle.
The flow is driven by a pressure gradient that is balanced by the Coriolis
force at the top of the domain. Buoyancy forces are active and the
k-epsilon ABL model of Andrey Sogachev et al. (DOI 10.1007/s10546-012-9726-5)
is used, as implemented by Tilman Koblitz and M. Paul van der Laan.
W-momentum is not solved in 1D and therefore there is no influence of
Buoyancy in the momentum equations. EllipSys3D shows that W is much smaller
than $`(U^2+V^2)^{1/2}`$, (order of 1e-6% above 1 m, and 0.1% below 1 m) for
the entire diurnal cycle, and can therefore be assumed to be zero.

## ml_neutral

This test case is the same as the gabls2 test case, where the mixing-length model
is used in combination with a prescribed limited length from Blackadar (1962).

## ml_unstable

This test case is the same as the gabls2 test case, where the mixing-length model
is used in combination with a prescribed limited length from Blackadar (1962),
extended to unstable conditions following MOST.

## channel

The canonical half-channel flow, where the flow is driven by a constant
pressure gradient force, $`F_p = u_*^2/L_z`$ (see Chapter 7, Pope 2000).
One can therefore set this force to obtain the desired friction velocity. This
type of flow has been utilized for ABL+wake flows (Abkar, PoF2015).
