### Input: grid.points                     ###########
### Assumes output order:                   ###########
###  u,v,w,tke,deps,uu,vv,ww,uv,uw,vw,      ###########
###  P,sigma,a11,a22,a33,a12,a13,a23        ###########
### Usage: gnuplot pline_test.p              ###########
#######################################################

set terminal x11 size 1300,1000
set multiplot layout 3,3 title "EllipSys1D grid.points results"
set grid

# Define constants
cmu_zeli = 0.0871717
cmu_abl = 0.03
ustar = 0.6

#### Plot velocity ##################
p "ke_abl/grid.points" u 4:3 with linespoints title "U (keps)",\
  "wjearsm/grid.points" u 4:3 with linespoints title "U (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 4:3 with linespoints title "U (WJ-EARSM-DC)"


#### Plot TKE ######################
set xrange [0.0:*]
p "ke_abl/grid.points" u 6:3 with linespoints title "tke (keps)",\
  "wjearsm/grid.points" u 6:3 with linespoints title "tke (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 6:3 with linespoints title "tke (WJ-EARSM-DC)", \
  "ke_abl/grid.points" u (ustar**2/sqrt(cmu_abl)):3 with lines title "ustar**2/sqrt(0.03)",\
  "wjearsm/grid.points" u (ustar**2/sqrt(cmu_zeli)):3 with lines title "ustar**2/sqrt(0.087)",\


### Plot dissipation of TKE #########
set xrange [0:0.001]
p "ke_abl/grid.points" u 7:3 with linespoints title "eps (keps)",\
  "wjearsm/grid.points" u 7:3 with linespoints title "eps (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 7:3 with linespoints title "eps (WJ-EARSM-DC)", \

set xrange [*:*]
### Plot normal stresses ###########
p "ke_abl/grid.points" u 8:3 with linespoints title "uu (keps)",\
  "wjearsm/grid.points" u 8:3 with linespoints title "uu (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 8:3 with linespoints title "uu (WJ-EARSM-DC)", \

p "ke_abl/grid.points" u 9:3 with linespoints title "vv (keps)",\
  "wjearsm/grid.points" u 9:3 with linespoints title "vv (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 9:3 with linespoints title "vv (WJ-EARSM-DC)", \

p "ke_abl/grid.points" u 10:3 with linespoints title "ww (keps)",\
  "wjearsm/grid.points" u 10:3 with linespoints title "ww (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 10:3 with linespoints title "ww (WJ-EARSM-DC)", \


### Plot non-diagonal stresses ###########
p "ke_abl/grid.points" u 11:3 with linespoints title "uv (keps)",\
  "wjearsm/grid.points" u 11:3 with linespoints title "uv (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 11:3 with linespoints title "uv (WJ-EARSM-DC)", \

p "ke_abl/grid.points" u 12:3 with linespoints title "uw (keps)",\
  "wjearsm/grid.points" u 12:3 with linespoints title "uw (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 12:3 with linespoints title "uw (WJ-EARSM-DC)", \

p "ke_abl/grid.points" u 13:3 with linespoints title "vw (keps)",\
  "wjearsm/grid.points" u 13:3 with linespoints title "vw (WJ-EARSM)", \
  "wjearsm_dc/grid.points" u 13:3 with linespoints title "vw (WJ-EARSM-DC)", \




### Finish multiplot
unset multiplot

### Wait 999 seconds before plot is closed ####
pause 999


