# GABLS1 case

A famous testcase for ABL models. It is a stably-stratified ABL, which reaches a quasi-stationary state after around 9 hrs.

References:
- Beare et al. (BLM2006), original LES intercomparision.
- Cuxart et al. (BLM2006), original SCM intercomparision.
- Sullivan et al. (JAM2016), new high-resolution LES.

## Running and plotting the simulation

*In order to run the full 9 hrs, change mstep and transient in the `input.dat`-file first!*

The simulation can be run with the python script, `run_gabls1.py`.

        $ python run_gabls1.py


## Results

The results of the EllipSys1D simulation is below compared to the LES data of Sullivan et al. (2016).

![](keps_abl/figures/windspeed.png)
![](keps_abl/figures/temperature.png)
![](keps_abl/figures/hodograph.png)
![](keps_abl/figures/tke.png)
![](keps_abl/figures/winddir.png)
![](keps_abl/figures/heatflux.png)
![](keps_abl/figures/tot_momflux.png)
![](keps_abl/figures/gradient_ri.png)





