import os
import numpy as np
home = os.path.expanduser("~")
import sys
sys.path.append(home+'/git/ellipsys1d/tools/gp2netcdf/')
from gp2netcdf import gp2netcdf
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import cm
import xarray
import pickle
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['axes.formatter.useoffset'] = False
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams['font.size'] = 17
plt.rcParams['lines.linewidth'] = 3.0

'''
    The GABLS1 case (Beare et al., 2006)    

    This program will simply run the input.dat file as is and subsequently
    plot the results. 
'''



############################################################################
###################### SIMULATION RUN ######################################
############################################################################
# Control flags (useful, if for example you have run the simulation and just want to work on plotting)
grid = 1       # Create symbolic link to grid-file
run = 1        # Run the simulation
convert = 1    # 1=convert and write data to netCDF file, 0=read netCDF-file


# Link to grid
if grid == 1:
    os.system('ln -s ../grid/grid.X1D .')

# Run the e1d Fortran executable:
if run == 1:
    flowfield = home + '/git/ellipsys1d/Executables/flowfield'
    os.system('rm grid.points')
    os.system(flowfield + ' input.dat')

# Convert grid.points to netCDF
if convert == 1:
    data = gp2netcdf(write=1,dt=1.0)
else:
    data = xarray.load_dataset('points.nc',decode_times=False)


# Add some derived variables (wind speed, wind direction, etc..)
data = data.assign({'speed': np.sqrt(data['u']**2+data['v']**2),
                    'winddir': np.arctan2(data['v'],data['u'])*180/np.pi
                    })
Nt = len(data.t)
Nz = len(data.z)
S2 = np.zeros((Nt,Nz))
N2 = np.zeros((Nt,Nz))
Ri = np.zeros((Nt,Nz))
g = 9.81
theta0 = 263.5
for i in range(Nt):
    S2[i] = np.gradient(data['u'][i].values,data.z.values)**2 + np.gradient(data['v'][i].values,data.z.values)**2
    N2[i] = g/theta0*np.gradient(data['temperature'][i].values,data.z.values)
    Ri[i] = N2[i]/(S2[i]+1e-30)
data = data.assign({'S2': xarray.DataArray(data=S2, dims=("t","z"), coords={"t": data.t.values,"z": data.z.values}),
                    'N2': xarray.DataArray(data=N2, dims=("t","z"), coords={"t": data.t.values,"z": data.z.values}),
                    'Ri': xarray.DataArray(data=Ri, dims=("t","z"), coords={"t": data.t.values,"z": data.z.values})
                    })



############################################################################
###################### PLOTTING ############################################
############################################################################
def conplot(x=None, y=None, var='None', lvls='None', xlabel=r'$t$ [hr]', ylabel=r'$z$ [m]', 
          varlabel=r'var', xlim='None', ylim='None', xloc='None', yloc='None'):
    """
        Create contour plot of var(t,z).
    """
    # Create plot
    fig = plt.figure()
    ax = plt.gca()
    p = ax.contourf(x, y, var, levels=lvls, cmap=cm.jet)
    fig.subplots_adjust(right=0.8)
    cbar = fig.add_axes([0.85, 0.15, 0.03, 0.7])
    plt.colorbar(p, cax=cbar, aspect=100)
    cbar.set_ylabel(varlabel,rotation=0,va='center',ha='left')
    
    # Set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel,rotation=0,va='center',ha='right')

    # Set limits
    if xlim == 'None':
        pass
    else:    
        ax.set_xlim(xlim[0],xlim[1])
    if ylim == 'None':
        pass
    else:    
        ax.set_ylim(ylim[0],ylim[1])
        
    # Set minor ticks
    if xloc == 'None':
        pass
    else:
        plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(xloc))
    if yloc == 'None':
        pass
    else:
        plt.gca().yaxis.set_minor_locator(plt.MultipleLocator(yloc))
        
    return fig, ax


# Contour plots (like Fig.6 in van der Laan and Sørensen, 2017)
T, Z = np.meshgrid(data.t/60/60, data.z)
t1 = 0; t2 = 9.2   # start and end hour
z1 = 0; z2 = 400        # bottom and top height
conplot(T, Z, data['speed'].T, varlabel='$U$ [m/s]', lvls=np.arange(0,10.1,1),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['winddir'].T, varlabel=r'$\varphi$ [$^\circ$]', lvls=np.arange(-5,45.1,6),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['tke'].T, varlabel=r'$k$ [m$^2$/s$^2$]', lvls=np.arange(0,0.501,0.05),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['temperature'].T, varlabel=r'$\Theta$ [$^\circ$K]', lvls=np.arange(262,268.1,0.5),
        xlim=[t1,t2], ylim=[z1,z2])


def lineplot(lines, xlabel=r'$x$', ylabel=r'$z$ [m]', 
             xlim='None', ylim='None', xloc='None', yloc=25):
    """
        Create a plot with a lines
    """
    # Create plot
    fig = plt.figure()
    ax = plt.gca()
    for i in range(len(lines)):
        plt.plot(lines[i]['x'], lines[i]['y'], lines[i]['ls'], label=lines[i]['label'])
    
    # Set labels
    plt.xlabel(xlabel)
    plt.ylabel(ylabel,rotation=0,va='center',ha='right')

    # Set limits
    if xlim == 'None':
        pass
    else:    
        plt.xlim(xlim[0],xlim[1])
    if ylim == 'None':
        pass
    else:    
        plt.ylim(ylim[0],ylim[1])
        
    # Set minor ticks
    if xloc == 'None':
        pass
    else:
        plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(xloc))
    if yloc == 'None':
        pass
    else:
        plt.gca().yaxis.set_minor_locator(plt.MultipleLocator(yloc))

    # Set legend
    plt.legend(handlelength=1.6)    
        
    return fig, ax

# Load reference LES
with open('../sullivan2016.pkl', 'rb') as fp:
    les = pickle.load(fp)

# For all line plots
ls = ['k-','b--', 'r:']
tfirst = data.t.values[0]/60/60
tlast = data.t.values[-1]/60/60
labels = ['LES','e1d ($t=%.1f$ hr)'%tfirst, 'e1d ($t=%.1f$ hr)'%tlast]
z1 = 0; z2=250

# Wind speed
fig, ax = lineplot([{'x': les['speed'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['speed'][0],  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': data['speed'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r'$\mathbb{V}~$ [m/s]', xloc=1.0, ylim=[z1,z2])
fig.savefig('figures/windspeed.png', bbox_inches='tight',dpi=150)

# Temperature
fig, ax = lineplot([{'x': les['temperature'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['temperature'][0],  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': data['temperature'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r'$\Theta$ [$^\circ$K]', xloc=0.5, xlim=[262.5,266.5], ylim=[z1,z2])
fig.savefig('figures/temperature.png', bbox_inches='tight',dpi=150)

# Hodograph
fig, ax = lineplot([{'x': les['u'],  'y': les['v'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['u'][0],  'y': data['v'][0],  'label': labels[1], 'ls': 'bo'},
          {'x': data['u'][-1], 'y': data['v'][-1], 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r'$U$ [m/s]', ylabel=r'$V$ [m/s]', xloc=1, yloc=0.5)
fig.savefig('figures/hodograph.png', bbox_inches='tight',dpi=150)

# TKE
fig, ax = lineplot([{'x': les['tke'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['tke'][0],  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': data['tke'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r'$k$ [m$^2$/s$^2$]', xloc=0.05, ylim=[z1,z2])
fig.savefig('figures/tke.png', bbox_inches='tight',dpi=150)

# Wind direction
fig, ax = lineplot([{'x': les['winddir'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['winddir'][0],  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': data['winddir'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r'$\varphi$ [$^\circ$]', xloc=5, xlim=[-5,40], ylim=[z1,z2])
fig.savefig('figures/winddir.png', bbox_inches='tight',dpi=150)

# Vertical heat flux
fig, ax = lineplot([{'x': les['wt'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['heatflux'][0],  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': data['heatflux'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r"$\overline{\theta'w'}$ [$^\circ$K m/s]", xloc=0.0025, xlim=[-0.015,None], ylim=[z1,z2])
fig.savefig('figures/heatflux.png', bbox_inches='tight',dpi=150)

# Total momentum flux
fig, ax = lineplot([{'x': -np.sqrt(les['uw']**2+les['vw']**2),  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': -data['uf'][0]**2,  'y': data.z, 'label': labels[1], 'ls': ls[1]},
          {'x': -data['uf'][-1]**2, 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r"$\overline{u'w'}^T$ [m$^2$/s$^2$]", xloc=0.01, ylim=[z1,z2])
fig.savefig('figures/tot_momflux.png', bbox_inches='tight',dpi=150)

# Gradient Richardson number
fig, ax = lineplot([{'x': les['Ri'],  'y': les['z'], 'label': labels[0], 'ls': ls[0]},
          {'x': data['Ri'][-1], 'y': data.z, 'label': labels[2], 'ls': ls[2]}], 
         xlabel=r"Ri [-]", xloc=0.05, xlim=[0,0.31], ylim=[z1,z2])
fig.savefig('figures/gradient_ri.png', bbox_inches='tight',dpi=150)

