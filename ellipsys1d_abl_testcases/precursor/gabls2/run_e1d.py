
import os
import numpy as np
home = os.path.expanduser("~")
import sys
sys.path.append(home+'/git/ellipsys1d/tools/gp2netcdf/')
from gp2netcdf import gp2netcdf
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import cm
import xarray
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['axes.formatter.useoffset'] = False
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams['font.size'] = 17

'''
    This program will simply run the input.dat file as is and subsequently
    plot the results. 
    
    Before running, you need to make two changes:
        - Set mstep to 86400 in input.dat
        - Set transient true to 10.0 in input.dat
    
    This will make the simulation run for 10 days of real time (instead of
    just 10 min).
    
'''



############################################################################
###################### SIMULATION RUN ######################################
############################################################################
grid = 1
run = 1
convert = 1


# Link to grid
if grid == 1:
    os.system('ln -s ../grid/grid.X1D .')

# Run the e1d Fortran executable:
if run == 1:
    flowfield = home + '/git/ellipsys1d/Executables/flowfield'
    os.system('rm grid.points')
    os.system(flowfield + ' input.dat')

# Convert grid.points to netCDF
if convert == 1:
    data = gp2netcdf(write=1,dt=10.0)
else:
    data = xarray.load_dataset('points.nc',decode_times=False)


# Add some derived variables (wind speed, wind direction, etc..)
data = data.assign({'speed': np.sqrt(data['u']**2+data['v']**2),
                    'winddir': np.arctan2(data['v'],data['u'])*180/np.pi - 45
                    })





############################################################################
###################### PLOTTING ############################################
############################################################################
def conplot(x=None, y=None, var='None', lvls='None', xlabel=r'$t$ [hr]', ylabel=r'$z$ [m]', 
          varlabel=r'var', xlim='None', ylim='None', xloc='None', yloc='None'):
    """
        Create contour plot of var(t,z).
    """
    # Create plot
    fig = plt.figure()
    ax = plt.gca()
    p = ax.contourf(x, y, var, levels=lvls, cmap=cm.jet)
    fig.subplots_adjust(right=0.8)
    cbar = fig.add_axes([0.85, 0.15, 0.03, 0.7])
    plt.colorbar(p, cax=cbar, aspect=100)
    cbar.set_ylabel(varlabel,rotation=0,va='center',ha='left')
    
    # Set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel,rotation=0,va='center',ha='right')

    # Set limits
    if xlim == 'None':
        pass
    else:    
        ax.set_xlim(xlim[0],xlim[1])
    if ylim == 'None':
        pass
    else:    
        ax.set_ylim(ylim[0],ylim[1])
        
    # Set minor ticks
    if xloc == 'None':
        pass
    else:
        plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(xloc))
    if yloc == 'None':
        pass
    else:
        plt.gca().yaxis.set_minor_locator(plt.MultipleLocator(yloc))
        
    return fig, ax


# Contour plots (like Fig.6 in van der Laan and Sørensen, 2017)
T, Z = np.meshgrid(data.t/60/60, data.z)
t1 = 9*24; t2 = 10*24   # start and end hour
z1 = 0; z2 = 800        # bottom and top height
conplot(T, Z, data['speed'].T, varlabel='$U$ [m/s]', lvls=np.arange(0,14.1,1),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['winddir'].T, varlabel=r'$\varphi$ [$^\circ$]', lvls=np.arange(-27,63.1,6),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['tke'].T, varlabel=r'$k$ [m$^2$/s$^2$]', lvls=np.arange(0,0.901,0.05),
        xlim=[t1,t2], ylim=[z1,z2])
conplot(T, Z, data['temperature'].T, varlabel=r'$\Theta$ [$^\circ$K]', lvls=np.arange(270,292.1,1),
        xlim=[t1,t2], ylim=[z1,z2])

