# Atmospheric boundary layer test cases

All test cases consist of a line grid, used to calculate profiles
of the atmospheric boundary layer (ABL).

## Grid

The grid consists of 192 cells and the domain is 6 km tall.
The bottom boundary condition (BC) is a rough wall, while the top BC depends on the case:

- In the analytic_profiles cases, the top is an inlet BC.
- In the precursor cases, the top is a symmetry BC.
