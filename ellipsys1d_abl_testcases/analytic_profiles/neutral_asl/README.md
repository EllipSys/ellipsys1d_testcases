# Neutral atmospheric surface layer

Also known as the "log-law" or "logarithmic" profiles due to the shape of the velocity profile.


#### Parameters
- Height of domain, $`L_z = 6000`$ m
- Friction velocity, $`u_* = 0.61`$ m/s
- Aerodynamic roughness length, $`z_0 = 0.072`$ m
- First wall-adjacent cell height, $`\Delta_1 = 1.06 \cdot 10^{-4}`$ m

#### Top boundary condition
- $`U`$, $`k`$ and $`\varepsilon`$: Dirichlet according to the analytical log-law expressions.

#### Bottom boundary condition
- Rough wall BC, see description by Sørensen et al. (2007).

#### Turbulence models

- The standard $`k`$-$`\varepsilon`$ model.
- The $`k`$-$`\varepsilon`$-$`f_P`$ model. In the logarithmic solution, $`f_P`$
should be close to 1.
- The 2D WJ-EARSM.
- The 2D WJ-EARSM with $`c_1=4.0`$.

#### Results

Produced with the pline_plot.p gnuplot script.

![](loglaw_comparison.png)

Some observations:
- The $`k`$-$`\varepsilon`$ model predicts significantly different profiles compared to the expected analytical solution for this case. This is presumably connected with the low value of $`\Delta_1 / z_0 = 0.0015`$, but could also be caused by the wall problems discussed by Blocken et al. (2007). Should be investigated further.
- The WJ-EARSM has $`\overline{u'u'} > \overline{v'v'} > \overline{w'w'}`$, while the $`k`$-$`\varepsilon`$ model has $`\overline{u'u'} = \overline{v'v'} = \overline{w'w'}`$. Increasing $`c_1`$ in the WJ-EARSM leads to a more equal distribution of TKE between the components, i.e., a more isotropic solution.
