# Analytic cases

These cases have analytical solutions, which are used to the set the values of the variables at the top BC. They are still relevant to simulate, because the numerical solution differ slightly from the analytical one. Hence, in 3D simulations, one can avoid freestream development by using the numerical solution instead of the analytical one.

### Ekman

This test case calculates the Ekman spiral, which is a laminar
solution of an ABL including Coriolis forces.

### Ellison

This test case calculates the Ellison solution, which is similar to the Ekman spiral, but with a linearly increasing eddy viscosity.


### Neutral ASL

This test case is a simple logarithmic profile, resembling the
neutral surface layer of the ABL (without Coriolis).

### Unstable/stable ASL

These test cases simulate unstable/stable surface layer profiles defined by
Monin-Obukhov Similarity Theory (MOST). The non-neutral profile
is in balance with a modified $k$-$\varepsilon$ model that is consistent
with MOST, see van der Laan et al: (DOI: 10.1002/we.2017).
