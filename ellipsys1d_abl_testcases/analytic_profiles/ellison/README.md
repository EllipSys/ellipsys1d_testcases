# Ellison solution

The Ekman equations are:
$$
\begin{aligned}
0 &= -f (V_g - V) + \left(\nu_t \frac{dU}{dz} \right)   \\
0 &= f (U_g - U) + \left(\nu_t \frac{dV}{dz} \right)
\end{aligned}
$$


An analytical solution exist for $\nu_t = \textrm{constant} \cdot z$, which is known as the *Ellison solution*. For details, see references.

References:
- Krishna (1980), doi: 10.1007/BF00120593
- van der Laan et al. (2020), doi: 10.5194/wes-5-355-2020

## Top boundary condition

An inlet BC is used at the top of the domain, hence we need to specify $U$ and $V$ there.

It is important to recognize that in the Ellison solution $U(z=H) \neq U_G$ (they are only equal for $z \rightarrow \infty$). For a large height, e.g., $H = 6$ km, we have $U(z=H) \approx U_G$, but they are still not exactly identical, which means that setting $U_{top} = U_G$ in the simulation would introduce a small error. We therefore first evaluate $U(z=H)$ from the analytical Ellison solution and set this at the top of our numerical domain.

## Running and plotting the simulation

The simulation can be run with the python script, `run_ellison.py`.

        $ python run_ellison.py


## Results

![](figures/u.png)
![](figures/v.png)
![](figures/nut.png)
![](figures/windspeed.png)
![](figures/winddir.png)
![](figures/hodograph.png)






