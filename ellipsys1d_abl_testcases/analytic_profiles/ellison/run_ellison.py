import os
import numpy as np
home = os.path.expanduser("~")
import sys
sys.path.append(home+'/git/ellipsys1d/tools/gp2netcdf/')
from gp2netcdf import gp2netcdf
import matplotlib.pyplot as plt
import matplotlib as mpl
import xarray
from scipy.special import ker, kei  # Kelvin functions
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['axes.formatter.useoffset'] = False
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams['font.size'] = 17
plt.rcParams['lines.linewidth'] = 3.0

'''
    The Ekman equations have an analytical for nu_t = constant*z,
    which is called the "Ellison solution".

    This program will simply run the input.dat file as is and subsequently
    plot the results. 
'''



############################################################################
###################### SIMULATION RUN ######################################
############################################################################
# Control flags (useful, if for example you have run the simulation and just want to work on plotting)
grid = 1       # Create symbolic link to grid-file
run = 1        # Run the simulation
convert = 1    # 1=convert and write data to netCDF file, 0=read netCDF-file


# Link to grid
if grid == 1:
    os.system('ln -s ../grid/grid.X1D .')

# Run the e1d Fortran executable:
if run == 1:
    flowfield = home + '/git/ellipsys1d/Executables/flowfield'
    os.system('rm grid.points')
    os.system(flowfield + ' input.dat')

# Convert grid.points to netCDF
if convert == 1:
    data = gp2netcdf(write=1,dt=1.0)
else:
    data = xarray.load_dataset('points.nc',decode_times=False)

# Add some derived variables (wind speed, wind direction, etc..)
data = data.assign({'speed': np.sqrt(data['u']**2+data['v']**2),
                    'winddir': np.arctan2(data['v'],data['u'])*180/np.pi
                    })


###############################################
### Analytical Ellison solution
###############################################
def ellison_analytical(z,G,ustar,z0,f,kappa=0.4):
    '''
        Analytical Ellison solution.
        The wind direction is assumed to be 0 at the bottom of the
        domain (hence both Ug and Vg are non-zero).
        
        Another assumption is that z0 << ustar/f.
    '''
    gamma_e = 0.57721  # Euler-Mascheroni constant
    c = -((gamma_e+0.5*np.log(z0*f/(kappa*ustar)))**2 + (np.pi/4)**2)**(-0.5)
    Ug = c*G*(gamma_e + 0.5*np.log(z0*f/(kappa*ustar)))
    Vg = c*G*np.pi/4
    
    Z = 2*np.sqrt(z*f/(kappa*ustar))
    U = c*G*ker(Z) + Ug
    V = c*G*kei(Z) + Vg
    S = np.sqrt(U**2 + V**2)
    phi = np.arctan2(V,U)*180/np.pi
    return U,V,S,phi,Ug,Vg


def tanh_grid1(N,alpha,Lx=1,xstart=0):
    # Modified version of 
    # https://caefn.com/cfd/hyperbolic-tangent-stretching-grid
    # This one makes a small cell height at one end and a large cell
    # height at the other end.
    
    # Create x-array goes from -1 to 1, but is stretched
    x = np.zeros(2*N)
    for j in range(2*N):
        epsj = -1 + 2*j/(2*N-2)
        #epsj = 2*j/(2*N-1)
        x[j] = 1/alpha*np.tanh(epsj*np.arctanh(alpha))
    
    # Make it go from 0 to 2
    x = (x+1)
    
    # Scale by desired length
    x = x*Lx
    
    # Shift to desired start position
    x = x + xstart
    
    
    # Only return half (from fine to coarse) 
    xhalf = x[:N]
    
    return xhalf


H = 6000
G = 10.0
ustar = 0.36
z0 = 0.01
f = 0.0001
kappa = 0.4
#z_an = np.linspace(0,H,1000)
z_an = tanh_grid1(1000,0.999,Lx=H)
U_an,V_an,S_an,phi_an,Ug,Vg = ellison_analytical(z_an, 
                                                 G=G,
                                                 ustar=ustar,
                                                 z0=z0,
                                                 f=f,
                                                 kappa=kappa)



############################################################################
###################### PLOTTING ############################################
############################################################################
def lineplot(lines, xlabel=r'$x$', ylabel=r'$z$ [m]', 
             xlim='None', ylim='None', xloc='None', yloc=500):
    """
        Create a plot with a lines
    """
    # Create plot
    fig = plt.figure()
    ax = plt.gca()
    for i in range(len(lines)):
        plt.plot(lines[i]['x'], lines[i]['y'], lines[i]['ls'], label=lines[i]['label'])
    
    # Set labels
    plt.xlabel(xlabel)
    plt.ylabel(ylabel,rotation=0,va='center',ha='right')

    # Set limits
    if xlim == 'None':
        pass
    else:    
        plt.xlim(xlim[0],xlim[1])
    if ylim == 'None':
        pass
    else:    
        plt.ylim(ylim[0],ylim[1])
        
    # Set minor ticks
    if xloc == 'None':
        pass
    else:
        plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(xloc))
    if yloc == 'None':
        pass
    else:
        plt.gca().yaxis.set_minor_locator(plt.MultipleLocator(yloc))

    # Set legend
    plt.legend(handlelength=1.6)    
        
    return fig, ax



# For all line plots
ls = ['k-','b--']
labels = ['Analytical Ellison', 'e1d']
z1 = 0; z2=6000

# Wind speed
fig, ax = lineplot([{'x': S_an,  'y': z_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['speed'][-1], 'y': data.z, 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$\mathbb{V}~$ [m/s]', xloc=1.0, ylim=[z1,z2], xlim=[6,10.5])
fig.savefig('figures/windspeed.png', bbox_inches='tight',dpi=150)

# Streamwise velocity
fig, ax = lineplot([{'x': U_an,  'y': z_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['u'][-1], 'y': data.z, 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$U~$ [m/s]', xloc=1.0, ylim=[z1,z2], xlim=[6,10.5])
fig.savefig('figures/u.png', bbox_inches='tight',dpi=150)

# Lateral velocity
fig, ax = lineplot([{'x': V_an,  'y': z_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['v'][-1], 'y': data.z, 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$V~$ [m/s]', xloc=1.0, ylim=[z1,z2])
fig.savefig('figures/v.png', bbox_inches='tight',dpi=150)

# Hodograph
fig, ax = lineplot([{'x': U_an,  'y': V_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['u'][-1], 'y': data['v'][-1], 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$U$ [m/s]', ylabel=r'$V$ [m/s]', xloc=1, yloc=0.5)
fig.savefig('figures/hodograph.png', bbox_inches='tight',dpi=150)

# Wind direction
fig, ax = lineplot([{'x': phi_an,  'y': z_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['winddir'][-1], 'y': data.z, 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$\varphi$ [$^\circ$]', xloc=5, xlim=[-10,0], ylim=[z1,z2])
fig.savefig('figures/winddir.png', bbox_inches='tight',dpi=150)

# Eddy viscosity
fig, ax = lineplot([{'x': ustar*kappa*(z_an+z0),  'y': z_an, 'label': labels[0], 'ls': ls[0]},
          {'x': data['vis'][-1], 'y': data.z, 'label': labels[1], 'ls': ls[1]}], 
         xlabel=r'$\nu_t$ [m$^2$/s]', xloc=100, ylim=[z1,z2])
fig.savefig('figures/nut.png', bbox_inches='tight',dpi=150)