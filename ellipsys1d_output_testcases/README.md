# Test case description

These test cases only tests the output model, no equations are solved.
All test cases consists of a line grid.

## Grid

The grid consists of 192 cells and the domain is 6 km tall. 

## Case 1: output/extract-line

The output is a line between two specified 2 points and a specified interval.

## Case 2: output/extract-line-zcc

This test case is the same as Case 8, where the output is based on all 
cell centers.

## Case 3: output/extract-point

This test case is the same as Case 8, where the output is based on specified
points.

## Case 4: output/extract-line-zcc-gc

Same as Case 2 above, but where the bottom ghost cell and top ghost cell are also output. Note, that the ghost cells only contain information about the primary variables (u,v,tke,dtke,T).
