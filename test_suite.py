
import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS1D_PATH
if os.getenv('ELLIPSYS1D_PATH') == None:
    raise RuntimeError('ELLIPSYS1D_PATH environment variable not set!')

'''
Define test cases for the automatic testing.
Remember:
- "mstep" will overwrite the mstep in the actual input.dat-file.
- The specified variables should also be output in the input.dat-file.
- The grid folder should always be in the folder above (else add symbolic link there).
'''

######################################################################
# ------------------ ABL testcases (analytic) -----------------------#
######################################################################
class analytic_profiles_ekman(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/ekman',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class analytic_profiles_ellison(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/ellison',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class analytic_profiles_neutral_asl_ke(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/neutral_asl/ke_abl',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_neutral_asl_wjearsm(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/neutral_asl/wjearsm',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_neutral_kefp(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/neutral_asl/kefp',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke','pt_ext_vis'],
                         'nprocs': 1}


class analytic_profiles_mo_stable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/stable_asl/mo',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mo_unstable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/unstable_asl/mo',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[50000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mo_stable_fp(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/stable_asl/mo_fp',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mo_unstable_fp(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/unstable_asl/mo_fp',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[50000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mob_stable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/stable_asl/mob',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mob_unstable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/unstable_asl/mob',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mob_stable_fp(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/stable_asl/mob_fp',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class analytic_profiles_mob_unstable_fp(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/analytic_profiles/unstable_asl/mob_fp',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


######################################################################
# ------------------ ABL testcases (precursor) ----------------------#
######################################################################
class precursor_neutral_nocori(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/neutral_nocorirot',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class precursor_neutral(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/neutral',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class precursor_neutral_nocori_Nb(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/neutral_nocorirot_Nb',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class precursor_neutral_Nb(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/neutral_Nb',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class precursor_unstable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/unstable',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke'],
                         'nprocs': 1}


class precursor_gabls1(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/gabls1/keps_abl',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[600]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_temperature',\
                                      'pt_ext_tke','pt_ext_dtke','pt_ext_lmax',\
                                      'pt_ext_uf','pt_ext_heatflux','pt_ext_den'],
                         'tol': 5,
                         'nprocs': 1}


class precursor_gabls2(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/gabls2',
                         'inputfile':'input.dat',
                         'auxilaryfiles':['Twalllow.dat'],
                         'inputs':[{'mstep':[600]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_temperature',\
                                      'pt_ext_tke','pt_ext_dtke','pt_ext_lmax',\
                                      'pt_ext_uf','pt_ext_heatflux','pt_ext_den'],
                         'tol': 5,
                         'nprocs': 1}


class precursor_neutral_var_lmax(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/neutral_var_lmax',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[200]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke','pt_ext_dtke','pt_ext_lmax'],
                         'nprocs': 1}


class ml_precursor_neutral(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/ml_neutral',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_vis'],
                         'nprocs': 1}


class ml_precursor_unstable(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/ml_unstable',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[10000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_vis'],
                         'nprocs': 1}


class precursor_channel(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/channel/ke_abl',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke', 'pt_ext_dtke'],
                         'nprocs': 1}


class precursor_channel_wjearsm(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver':'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_abl_testcases/precursor/channel/wjearsm',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100000]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_tke', 'pt_ext_dtke'],
                         'nprocs': 1}


######################################################################
# ------------------ Output testcases -------------------------------#
######################################################################
class output_extract_line_zcc(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_output_testcases/output/extract-line-zcc',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class output_extract_line_zcc_gc(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_output_testcases/output/extract-line-zcc-gc',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class output_extract_line(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_output_testcases/output/extract-line',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class output_extract_point(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_output_testcases/output/extract-point',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


######################################################################
# ------------------ fparser testcases ------------------------------#
######################################################################
class fparser_minmax(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_fparser_testcases/fparser/minmax',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_vis','pt_ext_den'],
                         'nprocs': 1}


class fparser_sum(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_fparser_testcases/fparser/sum',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v','pt_ext_vis','pt_ext_den'],
                         'nprocs': 1}


class fparser_file(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_fparser_testcases/fparser/file',
                         'inputfile':'input.dat',
                         'auxilaryfiles':['u.dat','v.dat'],
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class fparser_functions(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_fparser_testcases/fparser/functions',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[1]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


######################################################################
# ------------------ Canonical testcases ----------------------------#
######################################################################
class canonical_homshear(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_canonical_testcases/homshear/wjearsm',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[800]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


class canonical_rothomshear(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys1d',
                         'flowfield':'flowfield',
                         'directory': 'ellipsys1d_canonical_testcases/rothomshear/ccWJ_Ro0.5',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[800]},
                                   {'project':'grid'}],
                         'variables':['pt_ext_u','pt_ext_v'],
                         'nprocs': 1}


if __name__ == '__main__':

    unittest.main()
