import numpy as np
import xarray
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import cm
from mchba.flowdata import create_barytriangle


# PLOT STYLE ##################################
mpl.style.use('classic')
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1
plt.rcParams['grid.linestyle'] = ':'
mpl.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.grid']=False
yd = dict(rotation=0, ha='right')
plt.close('all')


# Load grid
data = np.loadtxt('grid.x1d')
cc = data[:-1] + np.diff(data)/2


### Plot cells #############
fig, ax = plt.subplots(1,2,sharey=True,figsize=(6,5),gridspec_kw={'width_ratios': [1, 10]}) 
#fig = plt.figure(figsize=(0.5,5))
ax[0].set_ylabel('z [m]')
ax[0].set_ylim(bottom=-10.0, top=110)
for i in range(len(data)):
    ax[0].plot([0,1],[data[i],data[i]],'k',linewidth=0.5)
    if(i==len(data)-1): continue
    ax[0].plot([0.5,0.5],[cc[i],cc[i]],'ko')
ax[0].get_xaxis().set_visible(False)

# Ghost cells
ax[0].axhspan(-10, 0, color='gray', alpha=0.5)
ax[1].axhspan(-10, 0, color='gray', alpha=0.5)
ax[0].axhspan(100, 110, color='gray', alpha=0.5)
ax[1].axhspan(100, 110, color='gray', alpha=0.5)
ax[1].text(5,-5,'Ghost cell',fontsize=20,ha='center',va='center')
ax[1].text(5,105,'Ghost cell',fontsize=20,ha='center',va='center')

# Explaning arrow in "XKCD"-style
with plt.xkcd():
    ax[1].annotate(
        'Velocity profile\nis fixed in time',
        xy=(4, 50), arrowprops=dict(arrowstyle='->'), xytext=(-1.2, 65))

# Velocity profile
S = 0.1
ax[1].grid()
ax[1].set_xlabel('$U$ [m/s]')
ax[1].plot(S*cc, cc, 'o-')
fig.savefig('vel_profile.png',bbox_inches='tight')
