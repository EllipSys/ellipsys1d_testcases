# Rotating homogeneous shear flow

The extension from homogeneous shear flow to rotational homogeneous shear flow is conceptually difficult to grasp (e.g., where is the rotation axis and won't this effect the results?), but easy to implement:

```math
S_{ij} = \begin{bmatrix} 0 & 0 & \frac{1}{2}\frac{k}{\varepsilon} \mathcal{S} \\ 0 & 0 & 0 \\ \frac{1}{2}\frac{k}{\varepsilon} \mathcal{S}  & 0 & 0 \end{bmatrix} \quad , \quad \Omega_{ij} = \begin{bmatrix} 0 & 0 & \frac{1}{2}\frac{k}{\varepsilon} \mathcal{S} - \frac{k}{\varepsilon} \Omega_s \\ 0 & 0 & 0 \\ -\frac{1}{2}\frac{k}{\varepsilon} \mathcal{S} + \frac{k}{\varepsilon} \Omega_s & 0 & 0 \end{bmatrix} ,
```

where $`\Omega_s`$ is the system rotation. When $`\Omega_s = 0`$, it reduces to the standard homogeneous shear flow. The results will be determined by the non-dimensional rotation number, $`\textrm{Ro} =  \Omega_s/\mathcal{S}`$.

It can be recognized that two-equation models will have trouble simulating rotating homogeneous shear flow, because the Boussinesq hypothesis is independent of $`\Omega_{ij}`$, hence a two-equation model will not feel the system rotation!

![](sketch_rothomshear.png)

The test case is similar to a case simulated by Wallin+Johansson (IJHFF2002).

#### Parameters
- $`L_z = 100`$ m
- $`N = 10`$ cells (+ 2 ghost cells)
- Uniformly spacing, hence $`\Delta z = 10`$ m
- $`U(z) = \mathcal{S} z`$, where $`\mathcal{S} = 0.1~\mathrm{s}^{-1}`$. This gives $`U(z=0) = 0`$ and $`U(L_z) = 10`$ m/s.
- Initial turbulence state, $`\mathcal{S} \frac{k}{\varepsilon}= 3.4`$.
- Rotation number, $`Ro = \frac{\Omega_s}{\mathcal{S}} = 0.5`$.
- Normalized timestep, $`\Delta t^* \equiv \Delta t S = 0.1`$ (in physical time $`\Delta t = 1`$ s).
- Integrated in time using an implicit Euler scheme with 10 subiterations pr. timestep.
- Total simulation time is $`t^* = 80`$ (in physical time $`t = 800`$ s).
- Homogeneous shear flow is independent of $`\rho`$ and $`\nu`$, because no momentum equation is solved and because the diffusion terms in the turbulence transport equations are zero!

#### Top and bottom boundary condition
- $`k`$ and $`\varepsilon`$: Symmetry
- $`U`$ is kept fixed according to $`U(z)`$ profile. Note, that the linear profile should be continued beyond the boundary to the ghost cells.

## Turbulence models
- The standard k-eps model ("evm").
- The WJ-EARSM model without curvature correction ("iWJ"). The "i" stands for "inertial", because no curvature correction corresponds to an EARSM derived in an inertial system. One can also view this as setting $`A_0 = \infty`$.
- The WJ-EARSM model with curvature correction ("CC-WJ"). Has $`A_0 = -0.72`$.

This notation follows Wallin+Johansson (IJHFF2002). For details on the curvature correction and the $`A_0`$ parameter, also refer to the article of Wallin+Johansson (IJHFF2002).



## Results

The left plot shows the results from EllipSys1D, while the right plot is taken from Figure 3 in Wallin+Johansson (2002). It is clear that very similar results are obtained, which gives confidence in the EllipSys1D code implementation.

![](kcomparison.png)

Simulations with three other rotation numbers have also been conducted (the input files of these are not included in this repository) and the results are shown below:

![](effect_of_Ro.png)

Some observations:

- The EVM predicts the same evolution for any rotation number, because it is insensitive to $`\Omega_{ij}`$.
- The CC-WJ has the best comparision with the LES data.
- One can compare the plot with the equivalent Figure 3 from Wallin+Johansson (IJHFF2002) to see that identical results are obtained - this gives confidence in the EllipSys1D code implementation.
- The system rotation either enhance or dampen the turbulence evolution compared to the non-rotating case. For $`0 < Ro < 0.5`$ there is a destabilizing effect, while for $`Ro < 0`$ and $`Ro > 0.5`$ there is a stabilizing effect on the turbulence production. This has been discussed widely in the turbulence community - Tritton (1992) explained it as a shear-Coriolis instability using dispersed particle analysis, while Speziale et al. (1996) used linear stability analysis to explain the mechanism.


## References
<a name="Wallin2002">1</a>:
Stefan Wallin and Arne V. Johansson,
*Modelling streamline curvature effects in explicit algebraic Reynolds stress turbulence models*, International Journal of Heat and Fluid Flow,
2002

<a name="Tritton1992">2</a>:
D. J. Tritton,
*Stabilization and destabilization of turbulent shear flow in a rotating fluid*, Journal of Fluid Mechanics,
1992

<a name="Speziale1996">3</a>:
Charles G. Speziale,
Ridha Abid,
Gregory A. Blaisdell,
*On the consistency of Reynolds stress turbulence closures with hydrodynamic stability theory*, Physics of Fluids,
1996
