# Canonical test cases

Some of the most simplest flow cases are sometimes called "canonical flows" and there often
exists a wealth of experimental and DNS data for these flows. In some cases there
are even analytical asymptotes and they are therefore often used for model verification.
Many canonical flows can be simulated with a 1D solver and below are a list of these.

Implemented:
- Homogeneous shear flow (<sup>[1](#Pope2000)</sup>, p.154)
- Rotating homogeneous shear flow <sup>[2](#Wallin2002)</sup>.

Other interesting cases not implemented yet:
- Stratified homogeneous shear flow <sup>[3](#Lazeroms2013)</sup>.
- Plane strain flow (like homogeneous shear flow, but with nonzero diagonal elements instead)<sup>[4](#Taulbee1992)</sup>.
- Stagnation streamline flow <sup>[4](#Taulbee1992)</sup>.
- Natural convection in vertical channel with differentially heated walls <sup>[5](#Lazeroms2013b)</sup>.



## References
<a name="Pope2000">1</a>:
S. Pope
*Turbulent Flows*,
2000

<a name="Wallin2002">2</a>:
Stefan Wallin and Arne V. Johansson,
*Modelling streamline curvature effects in explicit algebraic Reynolds stress turbulence models*,
2002

<a name="Lazeroms2013">3</a>:
W. M. J. Lazeroms, G. Brethouwer, S. Wallin, and A. V. Johansson
*An explicit algebraic Reynolds-stress and scalar-flux model for stably stratified flow*,
2013

<a name="Taulbee1992">4</a>:
D. Taulbee
*An improved algebraic Reynolds stress model and corresponding nonlinear stress model*,
1992

<a name="Lazeroms2013b">5</a>:
W. M. J. Lazeroms
*Explicit algebraic models for turbulent flows with buoyancy effects*,
2013
