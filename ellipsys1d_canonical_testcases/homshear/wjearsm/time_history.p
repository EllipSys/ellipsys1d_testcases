#######################################################
### Plot time history of a extract-point    ###########
### Input: grid.points                      ###########
#######################################################


set terminal x11 size 1300,1000
set multiplot layout 3,3 title "EllipSys1D results"
set grid
set key top center

# Define constant
dUdy = 0.1
dt = 1.0

set xrange [0:80]

#### Plot velocity ##################
p "grid.points" u ($2*dt*dUdy):4 with linespoints title "u",\
  "grid.points" u ($2*dt*dUdy):5 with linespoints title "v"

#### Plot TKE ######################
p "grid.points" u ($2*dt*dUdy):6 with linespoints title "tke"

### Plot dissipation of TKE #########
p "grid.points" u ($2*dt*dUdy):7 with linespoints title "eps"

### Plot normal stresses ###########
#p "grid.points" u ($2*dt*dUdy):8 with linespoints title "uu"
#p "grid.points" u ($2*dt*dUdy):10 with linespoints title "ww"

### Plot non-diagonal stresses ###########
#p "grid.points" u ($2*dt*dUdy):12 with linespoints title "uw"

### Plot production to dissipation ratio
p "grid.points" u ($2*dt*dUdy):($15/$7) with linespoints title "P/eps"

### Shear parameter
p "grid.points" u ($2*dt*dUdy):16 with linespoints title "sigma"

### S*k/eps ####################################
p "grid.points" u ($2*dt*dUdy):(dUdy*$6/$7) with linespoints title "S*k/eps"

### Eddy viscosity
p "grid.points" u ($2*dt*dUdy):14 with linespoints title "nut"

### Plot normal anisotropy
p "grid.points" u ($2*dt*dUdy):17 with linespoints title "a11",\
 "grid.points" u ($2*dt*dUdy):18 with linespoints title "a22",\
 "grid.points" u ($2*dt*dUdy):19 with linespoints title "a33"

### Plot non-diagonal aniostropy
p "grid.points" u ($2*dt*dUdy):20 with linespoints title "a12",\
  "grid.points" u ($2*dt*dUdy):21 with linespoints title "a13",\
  "grid.points" u ($2*dt*dUdy):22 with linespoints title "a23"


### Finish multiplot
unset multiplot

### Wait 999 seconds before plot is closed ####
pause 999
