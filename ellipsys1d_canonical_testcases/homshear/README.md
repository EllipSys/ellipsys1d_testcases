# Homogeneous shear flow

Perhaps the simplest turbulent flow to simulate in RANS, but a conceptually strange one. This test case is similar to the one used by Baungaard et al. (2022), where more details can also be found. In homogeneous shear flow, the velocity profile is fixed (as shown below) and $`k`$ and $`\varepsilon`$ evolves in time.


![](../grid/vel_profile.png)



#### Parameters
- $`L_z = 100`$ m
- $`N = 10`$ cells (+ 2 ghost cells)
- Uniformly spacing, hence $`\Delta z = 10`$ m
- $`U(z) = \mathcal{S} z`$, where $`\mathcal{S} = 0.1~\mathrm{s}^{-1}`$. This gives $`U(z=0) = 0`$ and $`U(L_z) = 10`$ m/s.
- Initial turbulence state, $`\mathcal{S} \frac{k}{\varepsilon}= 3.4`$.
- Normalized timestep, $`\Delta t^* \equiv \Delta t S = 0.1`$ (in physical time $`\Delta t = 1`$ s).
- Integrated in time using an implicit Euler scheme with 10 subiterations pr. timestep.
- Total simulation time is $`t^* = 80`$ (in physical time $`t = 800`$ s).
- Homogeneous shear flow is independent of $`\rho`$ and $`\nu`$, because no momentum equation is solved and because the diffusion terms in the turbulence transport equations are zero!

#### Top and bottom boundary condition
- $`k`$ and $`\varepsilon`$: Symmetry
- $`U`$ is kept fixed according to $`U(z)`$ profile. Note, that the linear profile should be continued beyond the boundary to the ghost cells.


### Running and plotting the simulation

        $ cd ke_zeli
        $ flowfield input.dat
        $ gnuplot time_history.p


The non-dimensional quantities reach an asymptotic state after some time and this state has an analytic solution that depends on the turbulence model, see for example Baungaard et al. (2022).

![](ke_zeli/results.png)
