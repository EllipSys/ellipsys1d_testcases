# EllipSys1D test cases

This repository contains the test cases for EllipSys1D. Two methods to run the test cases are described below. Both methods assume that a `flowfield` executable has already been compiled (see readme of the EllipSys1D repository).

## Running a test case manually



    # Step 1: Navigate to a desired case
    $ cd ellipsys1d_abl_testcases/analytic_profiles/loglaw/ke_abl/

    # Step 2: Make symbolic link to the grid
    $ ln -s ../grid/grid.X1D .

    # Step 3: Run the case with your ellipsys1d executable
    $ flowfield input.dat


## Running all test cases with a python script

To use the automated test case runner, you need to have Python 2.7.x installed. Such a python environment can be created with conda. Afterwards, you need to install the `ellipsyswrapper` module into this environment.

First, check if you already have conda installed:

    $ which conda

If this returns something like `/home/<username>/anaconda3/condabin/conda`, then go to "With Anaconda already installed". Else, if it returns nothing, then you can install Anaconda or Miniconda (see "With Miniconda").

### With Miniconda

    # Step 1: Choose a location to install miniconda
    $ export CONDA_ENV_PATH=/path/to/miniconda
    $ export PATH=$CONDA_ENV_PATH/bin:$PATH

    # Step 2: Download miniconda
    $ wget --quiet \
    https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh && \
    bash Miniconda-latest-Linux-x86_64.sh -b -p $CONDA_ENV_PATH && \
    rm Miniconda-latest-Linux-x86_64.sh && \
    chmod -R a+rx $CONDA_ENV_PATH

    # Step 3: Create a python 2.7 environment
    $ conda update --quiet --yes conda \
      && conda create -y -n py27 python=2.7 \
      && /bin/bash -c "source activate py27 \
      && conda install pip numpy scipy nose hdf5 unittest2"

    # Step 4: Install the ellipsyswrapper
    $ source activate py27
    $ pip install git+https://gitlab.windenergy.dtu.dk/EllipSys/ellipsyswrapper.git@openmpi-1.10.2

The environment is activated with `source activate py27`.

### With Anaconda already installed

    # Step 1: Create a python 2.7 environment
    $ conda create -n py27 python=2.7 pip numpy scipy nose hdf5 unittest2

    # Step 2: Install the ellipsyswrapper
    $ conda activate py27
    $ pip install git+https://gitlab.windenergy.dtu.dk/EllipSys/ellipsyswrapper.git@openmpi-1.10.2

The environment is activated with `conda activate py27`. To deactivate it, use `conda deactivate`.





### Running the testsuite

Step 1: If not already activated, then activate the python 2.7 environment.

Step 2: Specify the `ELLIPSYS1D_PATH` environment variable with the path to the `flowfield` executable (`ellipsyswrapper` will use this path). For example:

    $ export ELLIPSYS1D_PATH=/home/<user>/git/ellipsys1d/Executables/

This needs to be executed every time a new terminal is opened (one could optionally add this to the .bashrc file).

Step 3: With the above steps completed you can now run the test suite. Simply navigate the top folder of this repository and invoke the following command:

    $ python test_suite.py

which should run all the test cases (takes approximately 1 min) and finalize with an `OK`. One can clean the simulation results with `git clean -fd` (be aware that this removes all untracked files).
